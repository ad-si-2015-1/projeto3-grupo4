package cliente;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import servidor.IntServidor;

//import servidor.ImpServidor;

public class ImpCliente implements IntCliente{

	Registry registry;
	private String nome;
	IntCliente intCliente;
	IntServidor intServidor;
	
	public void registrar(String nome){
		this.nome = nome;
		try {
			intCliente = (IntCliente) UnicastRemoteObject.exportObject(this, 0);
			registry = LocateRegistry.getRegistry();
			//verificar se a quantidade � menor que 4. 4 porque � um do servidor e 3 dos jogadores
			if (registry.list().length < 4) {
				registry.bind(nome, intCliente);
			} else {
				System.out.println("Servidor lotado");
			}
		} catch (Exception e) {
			System.out.println("Erro de conex�o: " + e);
			e.printStackTrace();
			System.exit(1);
		}
		
	}
	
	public Registry getRegistry() {
		return registry;
	}
	public String getNome(){
		return nome;
	}

	@Override
	public void recebeMsgServidor(String mensagem) throws RemoteException {
		System.out.println(mensagem);
		
	}
}

