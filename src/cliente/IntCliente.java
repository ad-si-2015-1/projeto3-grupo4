package cliente;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IntCliente  extends Remote{
	
	void recebeMsgServidor(String mensagem) throws RemoteException;
}
