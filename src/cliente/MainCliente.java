package cliente;

import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Scanner;
//import java.util.Vector;

import servidor.IntServidor;
//import servidor.Palavra;

public class MainCliente {

//	Vector<Palavra> palavras;
	 
	public static void main(String[] args) throws AccessException, RemoteException, NotBoundException {
		
		
		// TODO Auto-generated method stub
		ImpCliente implCliente = new ImpCliente();
		Scanner scanner = new Scanner(System.in);
		System.out.println("Insira seu nome");
		String nome = scanner.nextLine();
		implCliente.registrar(nome);
		
		IntServidor intServidor = (IntServidor)implCliente.getRegistry().lookup("Servidor");
		intServidor.registraJogador(implCliente.getNome());
		while (true) {
			String campo = scanner.nextLine();
			intServidor.recebeMsgCliente(campo, implCliente.getNome());
		}
	}
	
	

}
