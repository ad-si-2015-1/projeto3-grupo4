package servidor;

public class Rodada {  
	  
//	   private static Rodada instancia; //Declaramos um objeto privado e est�tico 'instancia'
	   private Palavra palavra;
	   private String vezJogada;
	  
//	   Rodada() {}  
	   
	   public Palavra getPalavra(){
		   return palavra;
	   }
	   
	   public void setPalavra(Palavra palavra){
		   this.palavra = palavra;
	   }
	   
	   public String getVezJogada(){
		   return vezJogada;
	   }
	   
	   public void setVezJogada(String vezJogada){
		  this.vezJogada = vezJogada;
	   }
	  
//	   public static Rodada getInstancia() { //M�todo (get) p�blico e est�tico de controle e acesso ao objeto e sua instancia��o  
//	      if (instancia == null){// Teste: 'se a vari�vel de refer�ncia estiver nula' > Instancie uma s� vez o objeto "instancia"  
//	    	  instancia = new Rodada();  
//	      }
//	      return instancia; // E a retorne!  
//	   }  
} 