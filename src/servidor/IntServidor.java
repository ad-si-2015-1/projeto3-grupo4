package servidor;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IntServidor extends Remote {

	void recebeMsgCliente(String msgCliente, String nomeCliente) throws RemoteException;
	void registraJogador(String nome) throws RemoteException;
	boolean validaLetra(Character charac) throws RemoteException;
	boolean validaPalavra(String palavra) throws RemoteException;
	public boolean verificaInicio()throws RemoteException;
	public void iniciarJogo()throws RemoteException;
//	public Palavra sorteiaPalavra() throws RemoteException;
	void sorteiaPalavra() throws RemoteException;
	
}
