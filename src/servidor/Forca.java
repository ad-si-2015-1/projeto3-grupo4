package servidor;

import java.io.IOException;
import java.util.Vector;

/**
 *
 * @author Diogo Leal
 */

public class Forca {
	int vez;
	Palavra palavra = new Palavra();
	Vector<Character> letrasUtilizadas = new Vector<Character>();
	Vector<Character> letrasContidas = new Vector<Character>();
	boolean letraRepetida = false;
	boolean fimRodada = false;

	public int getVez() {
		return vez;
	}

	public void setVez(int vez) {
		this.vez = vez;
	}

	// public Forca () {
	// setVez(0);
	// }

	// public void mostrarPalavra(){
	//
	// }

	public String tratarMensagem(String nome, String msg, String palavraSorteada) throws IOException {
		String resposta = "";
		char letraJogador;
		if (msg.length() != 1) {
			trocarVez(getVez());
			// Verifica se a entrada do jogador � igual a palavra em jogo
			if (msg.equals(palavraSorteada)) {
				resposta = "O vencedor foi o jogador "	+ nome + "! PARABENS!!!\n"; 
				resposta = resposta + "A palavra era: "	+ palavraSorteada + "\r\n"; 
				letrasUtilizadas.clear(); // limpa o vector para a nova palavra
				letrasContidas.clear(); // limpa o vector para a nova palavra
				fimRodada = true;
			} else {
				resposta = "Errou a palavra\n\r" + palavra.mascararPalavra(palavraSorteada, letrasContidas, letrasUtilizadas);
			}

			letraRepetida = false;
		} else {
			// verifica se o caracter de entrada do jogador esta contido na
			// palavra em jogo
			letraJogador = msg.charAt(0);
			for (int i = 0; i < letrasUtilizadas.size(); i++) {
				if (letraJogador == letrasUtilizadas.get(i)) {
					resposta = "Essa letra ja foi utilizada. Tente Novamente\n\r" + palavra.mascararPalavra(palavraSorteada, letrasContidas, letrasUtilizadas);
					letraRepetida = true;
				}
			}

			if (letraRepetida) {

			} else {
				letrasUtilizadas.add(letraJogador);
				if (palavraSorteada.contains(Character.toString(letraJogador))) {
					letrasContidas.add(letraJogador);
					resposta = "------------------------------------------------------\r\nVoce acertou, jogue de novo por favor.\r\n";
					resposta = resposta + palavra.mascararPalavra(palavraSorteada, letrasContidas, letrasUtilizadas);
				} else {
					resposta = "O jogador " + nome + " errou a letra e perdeu a vez.\n\r" + palavra.mascararPalavra(palavraSorteada, letrasContidas, letrasUtilizadas);
					trocarVez(getVez());
				}
			}
			letraRepetida = false;
		}

		int i = 0;
		return resposta;
	}

	public boolean verificaVez(int numJogador) {
		if (numJogador == vez) {
			return true;
		} else {
			return false;
		}
	}

	public void trocarVez(int vezatual) {
		if (vezatual != 3) {
			vezatual++;
			setVez(vezatual);
		} else {
			setVez(1);
		}
	}
}