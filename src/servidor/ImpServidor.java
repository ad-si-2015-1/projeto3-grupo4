package servidor;

import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Random;
import java.util.Vector;

import cliente.IntCliente;

public class ImpServidor implements IntServidor{
	
	private Registry registry;
	private boolean jogoComecou = false;
	Vector<Palavra> palavras;
	Rodada rodada = new Rodada();
	Palavra palavra = new Palavra();
	Forca forca = new Forca();
	String strPalavra;
	IntCliente intCliente;
	
	public void registrar() {
		try {
			registry = LocateRegistry.createRegistry(1099);
			IntServidor intServidor = (IntServidor) UnicastRemoteObject.exportObject(this, 0);
			iniciaPalavras();
			registry.bind("Servidor", intServidor);
			System.err.println("Server ready");
		} catch (Exception e) {
			System.out.println("Erro de conex�o: " + e);
			e.printStackTrace();
			System.exit(1);
		}
	}

	@Override
	public void registraJogador(String nome) throws RemoteException {
		// TODO Auto-generated method stub
		if (!jogoComecou) {
			if (registry.list().length < 4) {
				System.out.println("Cliente Conectado: "+nome);
			} else if (registry.list().length == 4){
				System.out.println("Cliente Conectado: "+nome);
				iniciarJogo();
			}
			
		}
	}

	@Override
	public boolean validaLetra(Character charac) throws RemoteException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean validaPalavra(String palavra) throws RemoteException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verificaInicio() throws RemoteException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void iniciarJogo() throws RemoteException {
		// TODO Auto-generated method stub
		jogoComecou = true;
		sorteiaPalavra();
		palavra = rodada.getPalavra();
		System.out.println(palavra.mascararPalavra(palavra.getNomePalavra(), forca.letrasContidas, forca.letrasUtilizadas));
		//Forca forca = new Forca();
	}

	@Override
	public void sorteiaPalavra() throws RemoteException {
		// TODO Auto-generated method stub
		iniciaPalavras();
		Palavra palavra = new Palavra();
		Random rand = new Random();
		palavra = getPalavras().get(rand.nextInt(getPalavras().size()));
		rodada.setPalavra(palavra);
	}
	
	public Vector<Palavra> getPalavras(){
		return palavras;
	}
	
	public void iniciaPalavras(){
		/**
	     * Inicializacao do vetor de palavras que poderao ser utilizadas.
	     * O primeiro atributo do vetor com a categoria da palavra
	     * O segundo e a palavra
	     */
         palavras = new Vector<Palavra>();
         palavras.add(new Palavra("animal","cachorro"));
         palavras.add(new Palavra("veiculo","carro"));
         palavras.add(new Palavra("fruta","morango"));
         palavras.add(new Palavra("objeto","cadeira"));
         palavras.add(new Palavra("objeto","mesa"));
         palavras.add(new Palavra("animal","gato"));
         palavras.add(new Palavra("animal","hipopotomo"));
         palavras.add(new Palavra("animal","elefante"));
         palavras.add(new Palavra("fruta","melancia"));
         palavras.add(new Palavra("veiculo","trem"));
         palavras.add(new Palavra("fruta","carambola"));
         palavras.add(new Palavra("objeto","teclado"));
         palavras.add(new Palavra("objeto","computador"));
         palavras.add(new Palavra("animal","papagaio"));
         palavras.add(new Palavra("animal","leopardo"));
         palavras.add(new Palavra("animal","leao"));
         palavras.add(new Palavra("fruta","pitanga"));
         palavras.add(new Palavra("veiculo","metro"));
         palavras.add(new Palavra("animal","galinha"));
         palavras.add(new Palavra("veiculo","bicicleta"));
         palavras.add(new Palavra("fruta","jobuticaba"));
         palavras.add(new Palavra("objeto","caneta"));
         palavras.add(new Palavra("objeto","armario"));
         palavras.add(new Palavra("animal","lagarto"));
         palavras.add(new Palavra("animal","peixe"));
         palavras.add(new Palavra("animal","orangotango"));
         palavras.add(new Palavra("fruta","laranja"));
         palavras.add(new Palavra("objeto","impressora"));
         palavras.add(new Palavra("objeto","espelho"));
         palavras.add(new Palavra("animal","pato"));
         palavras.add(new Palavra("animal","galo"));
         palavras.add(new Palavra("animal","tatu"));
         palavras.add(new Palavra("fruta","caja"));
         palavras.add(new Palavra("fruta","caju"));
         palavras.add(new Palavra("fruta","seriguela"));
         palavras.add(new Palavra("fruta","kiwi"));
         palavras.add(new Palavra("animal","girafa"));
         palavras.add(new Palavra("animal","rinoceronte"));
         palavras.add(new Palavra("animal","camelo"));
         palavras.add(new Palavra("animal","carneiro"));
	}

	@Override
	public void recebeMsgCliente(String msgCliente, String nomeCliente) throws RemoteException {
		// TODO Auto-generated method stub
		try {
			String resposta = forca.tratarMensagem(nomeCliente, msgCliente, palavra.getNomePalavra());
			System.out.println(resposta);
			//verificar se terminou a rodada
			if (forca.fimRodada) {
				//inicia um novo jogo
				iniciarJogo();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

	

